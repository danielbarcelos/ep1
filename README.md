# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto
* Listar Candidatos
* Iniciar Votação
* Voto em Branco
* Confirma
* Cancela Passo
* Mostra vencedor da eleição
* Implementação para o voto de outros Estados
## Bugs e problemas

Não é exibido numero de votos do candidato vencedor.

## Referências
* [Material disponível no moodle](http://aprender.ead.unb.br)
* [LearnCpp](http://LearnCpp.com)
* [Documentação do C++](http://www.cplusplus.com)