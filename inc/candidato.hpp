
#include <string>

using namespace std;

class Candidato {
private: 
    string nm_ue;
    string ds_cargo;
    int nr_candidato;
    string nm_urna_candidato;
    string nm_partido;
    string sg_uf;
public:
    Candidato();
    Candidato(string nm_ue, string ds_cargo, int nr_candidato, string nm_urna_candidato, string nm_partido, string sg_uf);
    string getNm_ue();
    void setNm_ue(string nm_ue);
    string getDs_cargo();
    void setDs_cargo(string ds_cargo);
    int getNr_candidato();
    void setNr_candidato(int nr_candidato);
    string getNm_urna_candidato();
    void setNm_urna_candidato(string nm_urna_candidato);
    string getNm_partido();
    void setNm_partido(string nm_partido);
    string getSg_uf();
    void setSg_uf(string sg_uf);
    string toString();
};