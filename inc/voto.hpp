#include <string>

using namespace std;

class Voto{
private:
    string eleitor;
    string tipoVoto;
    string estadoEleitor;
    int nroCandidato;
public:
Voto(string eleitor, string tipoVoto, string estadoEleitor, int nroCandidato);
void setEleitor(string eleitor);
void setTipoVoto(string tipoVoto);
void setNroCandidato(int nroCandidato);
void setEstadoEleitor(int estadoEleitor);
string getEleitor();
string getTipoVoto();
string getEstadoEleitor();
int getNroCandidato();
};