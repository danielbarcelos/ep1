#include "voto.hpp"

Voto::Voto(string eleitor, string tipoVoto, string estadoEleitor, int nroCandidato){
    this->eleitor = eleitor;
    this->tipoVoto = tipoVoto;
    this->nroCandidato = nroCandidato;
    this->estadoEleitor = estadoEleitor;
}
void Voto::setEleitor(string eleitor){
    this->eleitor = eleitor;
}
void Voto::setTipoVoto(string tipoVoto){
    this->tipoVoto = tipoVoto;
}
void Voto::setNroCandidato(int nroCandidato){
    this->nroCandidato = nroCandidato;
}
void Voto::setEstadoEleitor(int estadoEleitor){
    this->estadoEleitor = estadoEleitor;
}
string Voto::getEleitor(){
    return eleitor;
}
string Voto::getTipoVoto(){
    return tipoVoto;
}
string Voto::getEstadoEleitor(){
    return estadoEleitor;
}
int Voto::getNroCandidato(){
    return nroCandidato;
}