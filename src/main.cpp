#include <iostream>
#include "urna.hpp"

using namespace std;

void mostraMenu(){
    cout << "Escolha uma opção: " << endl;
    cout << "1- Consultar Candidato" << endl;
    cout << "2- Iniciar Votação" << endl;

    cout << "0- Sair" << endl;
}



int main() {
    Urna urna;
    int opcao;
    do{
        mostraMenu();
        cin >> opcao;
        
        string cargo;
        switch(opcao){
            case 1:{
                int nroCandidato, opcaoCargo;
                string cargoCandidato;
                cout << "Insira o numero do candidato: ";
                cin >> nroCandidato;
                cout << "Insira o cargo do candidato:\n";
                cout << "1 - Presidente e vice" << endl << "2 - Governador e Vice" << endl <<
                "3 - Senador" << endl << "4 - Deputado Estadual" << endl << "5 - Deputado Distrital" <<
                endl << "6 - Deputado Federal" << endl;
                cin >> opcaoCargo;
                switch(opcaoCargo){
                    case 1:{
                        cargoCandidato = "PRESIDENTE";
                        break;
                    }
                    case 2:{
                        cargoCandidato = "GOVERNADOR";
                        break;
                    }
                    case 3:{
                        cargoCandidato = "SENADOR";
                        break;
                    }
                    case 4:{
                        cargoCandidato = "DEPUTADO ESTADUAL";
                        break;
                    }
                    case 5:{    
                        cargoCandidato = "DEPUTADO DISTRITAL";
                        break;
                    }
                    case 6:{
                        cargoCandidato = "DEPUTADO FEDERAL";
                        break;
                    }
                }
                urna.mostraCandidato(nroCandidato, cargoCandidato);
                break;
            }
            case 2:{
                urna.votacao();
                break;
            }
        }
            
    }while(opcao != 0);
    return 0;
}
