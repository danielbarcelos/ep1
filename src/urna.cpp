#include "urna.hpp"

using namespace std;

Urna::Urna(){
    candidatos = new vector<Candidato>();
    carregaCandidatos();
}

bool Urna::mostraCandidato(int nroCandidato, string ds_cargo){
    bool valido = false;
    if(nroCandidato == -1){
        cout << "Não houve ganhadores/votos!" << endl;
        return valido;
    }
    
    for(unsigned int i = 0; i < candidatos->size(); i++){
        Candidato atual = candidatos->at(i);
        if(atual.getNr_candidato() == nroCandidato && atual.getDs_cargo().find(ds_cargo) != string::npos){
            valido = true;
            cout << atual.toString();        
        }
    }
    return valido;
}

bool Urna::mostraCandidato(int nroCandidato, string ds_cargo, string estado){
    bool valido = false;
    if(nroCandidato == -1){
        cout << "Não houve ganhadores/votos!" << endl;
        return valido;
    }
    for(unsigned int i = 0; i < candidatos->size(); i++){   
        Candidato atual = candidatos->at(i);
        if(atual.getNr_candidato() == nroCandidato && atual.getDs_cargo().find(ds_cargo) != string::npos && atual.getSg_uf().find(estado) != string::npos){
            valido = true;
            cout << atual.toString();
        }
    }
    return valido;
}

void Urna::criaVoto(int nroCandidato, string tipoCandidato, string estadoEleitor, string nomeEleitor){
    Voto votoAtual(nomeEleitor, tipoCandidato, estadoEleitor, nroCandidato);
    this->votos.push_back(votoAtual);
}

void Urna::votaPresidente( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para presidente (-1 para voto branco): ";
    cin >> nro;
    if(nro != -1){
        if(!this->mostraCandidato(nro, "PRESIDENTE")){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaPresidente(nomeEleitor, estadoEleitor);
        }   
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "PRESIDENTE", estadoEleitor, nomeEleitor);
        }
        else{
            votaPresidente(nomeEleitor, estadoEleitor);
        }
    }
}

void Urna::votaGovernador( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para governador (-1 para voto branco): ";
    cin >> nro;
        
    if(nro != -1){
        if(!this->mostraCandidato(nro, "GOVERNADOR", estadoEleitor)){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaGovernador(nomeEleitor, estadoEleitor);
        } 
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "GOVERNADOR", estadoEleitor, nomeEleitor);
        }
        else{
            votaGovernador(nomeEleitor, estadoEleitor);
        }
    }
}

void Urna::votaSenador( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para senador (-1 para voto branco): ";
    cin >> nro;
        
    if(nro != -1){
        if(!this->mostraCandidato(nro, "SENADOR", estadoEleitor)){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaSenador(nomeEleitor, estadoEleitor);
        } 
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "SENADOR", estadoEleitor, nomeEleitor);
        }
        else{
            votaSenador(nomeEleitor, estadoEleitor);
        }
    }
}

void Urna::votaDeputadoEstadual( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para deputado estadual (-1 para voto branco): ";
    cin >> nro;
        
    if(nro != -1){
        if(!this->mostraCandidato(nro, "DEPUTADO ESTADUAL", estadoEleitor)){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaDeputadoEstadual(nomeEleitor, estadoEleitor);
        } 
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "DEPUTADO ESTADUAL", estadoEleitor, nomeEleitor);
        }
        else{
            votaDeputadoEstadual(nomeEleitor, estadoEleitor);
        }
    }
}

void Urna::votaDeputadoDistrital( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para deputado distrital (-1 para voto branco): ";
    cin >> nro;
        
    if(nro != -1){
        if(!this->mostraCandidato(nro, "DEPUTADO DISTRITAL", estadoEleitor)){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaDeputadoDistrital(nomeEleitor, estadoEleitor);
        } 
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "DEPUTADO DISTRITAL", estadoEleitor, nomeEleitor);
        }
        else{
            votaDeputadoDistrital(nomeEleitor, estadoEleitor);
        }
    }
}

void Urna::votaDeputadoFederal( string nomeEleitor, string estadoEleitor){
    int nro;
    int confirma;
    bool mostraOpcoes = true;

    cout << "Insira o numero do candidato para deputado federal (-1 para voto branco): ";
    cin >> nro;
        
    if(nro != -1){
        if(!this->mostraCandidato(nro, "DEPUTADO FEDERAL", estadoEleitor)){
            cout << "\n\nCandidato não existe!\n";
            mostraOpcoes = false;
            votaDeputadoFederal(nomeEleitor, estadoEleitor);
        } 
    }
    if(mostraOpcoes){
        cout << "1 - Confirma\n2- Cancela Passo" << endl;
        cin >> confirma;
        if(confirma == 1){
            criaVoto(nro, "DEPUTADO FEDERAL", estadoEleitor, nomeEleitor);
        }
        else{
            votaDeputadoFederal(nomeEleitor, estadoEleitor);
        }
    }
}


void Urna::votacao(){
    int nrele;
    int i;

    string nomeEleitor(""), estadoEleitor("");

    this->votos.clear();

    cout << "Insira a quantidade de eleitores: ";
    cin >> nrele;
    for(i=0; i<nrele; i++) {
        cout << "Insira o nome do eleitor: ";
        cin >> nomeEleitor;
        cout << "Insira o estado do eleitor: ";
        cin >> estadoEleitor;
        transform(estadoEleitor.begin(), estadoEleitor.end(), estadoEleitor.begin(), ::toupper);
        
        votaPresidente(nomeEleitor, estadoEleitor);
        votaGovernador(nomeEleitor, estadoEleitor);
        votaSenador(nomeEleitor, estadoEleitor);
        if(estadoEleitor != "DF"){
            votaDeputadoEstadual(nomeEleitor, estadoEleitor);
        }
        else{
            votaDeputadoDistrital(nomeEleitor, estadoEleitor);
        }
        votaDeputadoFederal(nomeEleitor, estadoEleitor);
    }
    
    mostraVencedor(estadoEleitor);
}

void Urna::mostraVencedor(string estadoEleitor){
    vector<Voto> presidentes;
    vector<Voto> governadores;
    vector<Voto> senadores;
    vector<Voto> deputadosDistritais;
    vector<Voto> deputadosEstaduais;
    vector<Voto> deputadosFederais;
    
    for(unsigned int i = 0; i < this->votos.size(); i++){
        Voto votoAtual = this->votos.at(i);
        if(votoAtual.getTipoVoto() == "PRESIDENTE"){
            presidentes.push_back(votoAtual);
        }
        else if(votoAtual.getTipoVoto() == "GOVERNADOR"){
            governadores.push_back(votoAtual);
        }
        else if(votoAtual.getTipoVoto() == "SENADOR"){
            senadores.push_back(votoAtual);
        }   
        else if(votoAtual.getTipoVoto() == "DEPUTADO DISTRITAL"){
            deputadosDistritais.push_back(votoAtual);
        }
        else if(votoAtual.getTipoVoto() == "DEPUTADO ESTADUAL"){
            deputadosEstaduais.push_back(votoAtual);
        }
        else if(votoAtual.getTipoVoto() == "DEPUTADO FEDERAL"){
            deputadosFederais.push_back(votoAtual);
        }
    }

    cout << "Presidente que ganhou: " << endl;
    mostraCandidato(contaVotos(presidentes), "PRESIDENTE");
    cout << "Governador que ganhou: " << endl;
    mostraCandidato(contaVotos(governadores), "GOVERNADOR", estadoEleitor);
    cout << "Senador que ganhou: " << endl;
    mostraCandidato(contaVotos(senadores), "SENADOR", estadoEleitor);
    cout << "Deputado Estadual que ganhou: " << endl;
    mostraCandidato(contaVotos(deputadosEstaduais), "DEPUTADO ESTADUAL", estadoEleitor);
    cout << "Deputado Distrital que ganhou: " << endl;
    mostraCandidato(contaVotos(deputadosDistritais), "DEPUTADO DISTRITAL", estadoEleitor);
    cout << "Deputado Federal que ganhou: " << endl;
    mostraCandidato(contaVotos(deputadosFederais), "DEPUTADO FEDERAL", estadoEleitor);

}    


int Urna::contaVotos(vector<Voto> votos){
    int hashVotos[999999];
    int nroVotosCandidato = 0;
    int nroCandidato = 0;
    int votosBrancos = 0;
    bool empate = false;

    for(int i = 0; i < 999999; i++){
        hashVotos[i] = 0;
    }
    for(unsigned int i = 0; i < votos.size(); i++){
        int candidato = votos.at(i).getNroCandidato();
        if(candidato != -1){
            hashVotos[candidato]++;
        }
        else{
            votosBrancos++;
        }
    }
    
    for(int i = 0; i < 999999; i++){
        if(hashVotos[i] > nroVotosCandidato){
            nroVotosCandidato = hashVotos[i];
            nroCandidato = i;
            empate = false;
        }
        else if(hashVotos[i] == nroVotosCandidato){
            empate = true;
        }
    }
    nroVotosCandidato += votosBrancos;
    if(!empate && nroCandidato != 0) return nroCandidato;
    else return -1;
}

void Urna::carregaCandidatos(){
    DIR *diretorio;
    string caminhoArquivo = "./data/";
    struct dirent *entidade;
    if ((diretorio = opendir("./data/")) != NULL) {
        while ((entidade = readdir(diretorio)) != NULL) {
            if(strstr(entidade->d_name, ".csv") != NULL){
                string nomeArquivo = caminhoArquivo + entidade->d_name;
                ifstream arquivoAtual(nomeArquivo.c_str());
                string linhaAtual;
                getline(arquivoAtual, linhaAtual);
                while (arquivoAtual) {
                    if (!getline(arquivoAtual, linhaAtual)) break;
                    istringstream ss(linhaAtual);
                    vector<string> values;
                    while(ss){
                        string line;
                        if(!getline(ss, line, ';')) break;
                        values.push_back(line);
                    }

                    string sg_uf = values.at(10); //sg_uf
                    string nm_ue = values.at(12); //nm_ue
                    string ds_cargo = values.at(14); //ds_cargo
                    string  nm_urna_candidato = values.at(18); // nm_urna_candidato
                    int nr_candidato; 
                    try{ 
                        values.at(16).erase(0,1);
                        values.at(16).erase(values.at(16).size()-1);
                        sscanf(values.at(16).c_str(), "%d", &nr_candidato);
                    }catch(...){
                    }
                    string nm_partido = values.at(29); //nm_partido
                    Candidato candidatoAtual(nm_ue, ds_cargo, nr_candidato, nm_urna_candidato, nm_partido, sg_uf);
                    candidatos->push_back(candidatoAtual);
                }
            }
        }
    }
}