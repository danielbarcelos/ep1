#include "candidato.hpp"

Candidato::Candidato(){
    this->nm_ue = "";
    this->ds_cargo = "";
    this->nr_candidato = -1;
    this->nm_urna_candidato = "";
    this->nm_partido = "";
    this->sg_uf = "";
}
Candidato::Candidato(string nm_ue, string ds_cargo, int nr_candidato, string nm_urna_candidato, string nm_partido, string sg_uf){
    this->nm_ue = nm_ue;
    this->ds_cargo = ds_cargo;
    this->nr_candidato = nr_candidato;
    this->nm_urna_candidato = nm_urna_candidato;
    this->nm_partido = nm_partido;
    this->sg_uf = sg_uf;
}

string Candidato::getNm_ue(){
    return nm_ue;
}
void Candidato::setNm_ue(string nm_ue){
    this->nm_ue = nm_ue;    
}
string Candidato::getDs_cargo(){
    return ds_cargo;
}
void Candidato::setDs_cargo(string ds_cargo){
    this->ds_cargo = ds_cargo;    
}
int Candidato::getNr_candidato(){
    return nr_candidato;
}
void Candidato::setNr_candidato(int nr_candidato){
    this->nr_candidato = nr_candidato; 
}
string Candidato::getNm_urna_candidato(){
    return nm_urna_candidato;
}
void Candidato::setNm_urna_candidato(string nm_urna_candidato){
    this->nm_urna_candidato = nm_urna_candidato;   
}
string Candidato::getNm_partido(){
    return nm_partido;
}
void Candidato::setNm_partido(string nm_partido){
    this->nm_partido = nm_partido;
}
string Candidato::getSg_uf(){
    return sg_uf;
}
void Candidato::setSg_uf(string sg_uf){
    this->sg_uf = sg_uf;
}
string Candidato::toString(){
    int contador = 0;
    string saida = "";
    for(int i = 0; i < 70; i++){
        saida += "*";
    }
    saida += "\n";
    for(int i = 0; i < 2; i++){
        saida += "*";
        for(int i = 1; i < 69; i++){
            saida += " ";
        }
        saida += "*\n";
    }
    saida += "*";
    contador++;
    for(int i = 0; i < 3; i++){
        contador++;
        saida += " ";
    }
    for(int i = 0; i < 11; i++){
        saida += "*";
        contador++;
    }
    saida += "  Nome: ";
    contador+= 8;
    saida += this->nm_urna_candidato;
    contador += this->nm_urna_candidato.length();

    for(int i = 0; i < 10; i++){
        saida += " ";
        contador++;
    }
    saida += "Estado: ";
    saida += this->sg_uf;
    contador += 8 + this->sg_uf.length();
    for(int i = 0; i < 70-contador-1; i++){
        saida += " ";
    }
    saida += "*\n";
    contador = 1;
    saida += "*";
    for(int i = 0; i < 3; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    contador ++;
    for(int i = 0; i < 9; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    saida += "  Partido: ";
    saida += this->nm_partido;
    contador += 12 + this->nm_partido.length();
    for(int i = 0; i < 69-contador; i++){
        saida += " ";
    }
    saida += "*\n";
    contador = 1;
    saida += "*";
    for(int i = 0; i < 3; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    contador ++;
    for(int i = 0; i < 9; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    saida += "  Cargo: ";
    saida += this->ds_cargo;
    contador += 10 + this->ds_cargo.length();
    for(int i = 0; i < 69-contador; i++){
        saida += " ";
    }
    saida += "*\n";

    contador = 1;
    saida += "*";
    for(int i = 0; i < 3; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    contador ++;
    for(int i = 0; i < 9; i++){
        contador++;
        saida += " ";
    }
    saida += "*";
    saida += "  Numero: ";
    saida += std::to_string(this->nr_candidato);
    contador += 11 + std::to_string(this->nr_candidato).length();
    for(int i = 0; i < 69-contador; i++){
        saida += " ";
    }
    saida += "*\n";
    saida += "*";
    for(int i = 0; i < 3; i++){
        contador++;
        saida += " ";
    }
    for(int i = 0; i < 11; i++){
        saida += "*";
    }
    for(int i = 0; i < 70-16; i++){
        saida += " ";
    }
    saida += "*\n";
    for(int i = 0; i < 2; i++){
        saida += "*";
        for(int i = 1; i < 69; i++){
            saida += " ";
        }
        saida += "*\n";
    }
    for(int i = 0; i < 70; i++){
        saida += "*";
    }
    saida += "\n";
    return saida;
}